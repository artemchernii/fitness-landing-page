$(function () {

	$('#my-menu').mmenu({
		extensions: [
			'theme-black',
			'effect-menu-slide',
			'pagedim-black',
			'position-right'
		],
		navbar: {
			title: '<span class="Jo_m">Jo</span><span class="fit_m">Fitness</span>'
		}
	});

	var api = $('#my-menu').data('mmenu');
	var $icon = $('.hamburger');

	api.bind('open:finish', function () {
		setTimeout(function () {
			$icon.addClass('is-active');
		}, 10);
	}).bind('close:finish', function () {
		setTimeout(function () {
			$icon.removeClass('is-active');
		}, 10);
	});

	$('.carousel_membership').on('initialized.owl.carousel', function () {
		setTimeout(function () {
			carouselService()
		}, 100);
	});

	$('.carousel_membership').owlCarousel({
		loop: true,
		nav: true,
		dots: false,
		smartSpeed: 700,
		navText: ['<i class="fas fa-chevron-circle-left"></i>', '<i class="fas fa-chevron-circle-right"></i>'],
		responsiveClass: true,
		responsive: {
			0: {
				items: 1
			},
			800: {
				items: 2
			},
			1100: {
				items: 3
			}
		}
	});
	$('.reviews').owlCarousel({
		loop: true,
		items: 1,
		smartSpeed: 700,
		nav: false,
		dots: true

	});

	function carouselService() {
		$(".carousel_membership__item").each(function () {
			var ths = $(this);
			var thsH = ths.find(".carousel_membership__item-content").outerHeight();
			ths.find(".carousel_membership__item-img").css("min-height", thsH);
		});
	}
	carouselService();

	$('section .h2').each(function () {
		var ths = $(this);
		ths.html(ths.html().replace(/^(\S+)/, '<span>$1</span>'));
	});

	$('select').selectize({
		create: true
	});

	//E-mail Ajax Send
	$("form.callback").submit(function () {
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php",
			data: th.serialize()
		}).done(function () {
			$(th).find('.success').addClass('active').css('display', 'flex').hide().fadeIn();
			setTimeout(function () {
				$(th).find('.success').removeClass('active').fadeOut();
				th.trigger("reset");
			}, 2500);
		});
		return false;
	});
});